package net.sytes.rokkosan.randomplayer

/*
2022 Jul. 19.
2022 Jul. 16.
2022 Jun. 25.
2022 May 15.
2022 Apr. 30.
2022 Feb. 23.
Ryuichi Hashimoto.

端末内の、指定ディレクトリ内の、動画もしくは音声ファイルを再生するアプリ

起動時にファイル読み込みパーミッションを取得する。
メイン画面に配置するウィジェット
　「読み込みディレクトリ選択」
　「音声ファイルのみ・ビデオファイルのみ・音声＋ビデオファイル選択」
　「再生」
　「リプレイ」
*/

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import net.sytes.rokkosan.randomplayer.databinding.ActivityMainBinding
import net.sytes.rokkosan.randomsmbsoundplay.ListPlayedFilesDialogFragment
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.*
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    val PERMISSION_READ_EX_STOR: Int = 100
    var selectedDirString = ""
    private lateinit var selectedDirUri: Uri
    private var selectedDirUriFileSystem: String = ""
    private var selectedDirUriContentProvider: String = ""
    private var selectedVolumeDir: String = ""
    private var selectedRelativeDir: String = ""
    private val sharedPref: SharedPreferences by lazy {this.getPreferences(Context.MODE_PRIVATE)}
    val typeAudioVideo = 0
    val typeAudio = 2
    val typeVideo = 3
    private val myPermission = Manifest.permission.READ_EXTERNAL_STORAGE
    private var isCheckedNotAskAgain: Boolean = false
    val spinnerMediaType: Spinner by lazy { binding.spinnerMediaType }
    lateinit var typeOfMedia: MediaMyEnumType
    lateinit var appContext: Context
    var medType: Int by Delegates.notNull()
    private var medPath: String = ""

    enum class MediaMyEnumType {
        soundVideo, sound, video
    }

    // ファイル読み込みパーミッション取得
    //   registerForActivityResult()を定義してlaunch()を呼び出すと、
    //   パーミッションが許可されていない時はユーザーにパーミッション許可を促すダイアログが表示される。
    //   パーミッションが「許可された時」「許可されなかった時」のコールバック関数を記述する
    private val permisLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { granted ->
        if (granted) {
            // パーミッションが取得されている時
            // パーミッション取得後にすべき処理に移る
            isCheckedNotAskAgain = false
            sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).apply()
            workWithPermission()

        } else {
            // パーミッションを取得していない
            if (shouldShowRequestPermissionRationale(myPermission)) {
                // パーミッション取得を拒否され、
                // 「今後表示しない」は選択されていない
                isCheckedNotAskAgain = false
                sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).apply()
            } else {
                // パーミッション取得を拒否され、
                // 「今後表示しない」が選択されて再リクエストも拒否されている
                isCheckedNotAskAgain = true
                sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).apply()
            }
            workWithoutPermission()
        }
    }

    // ファイル・ピッカーによるディレクトリ取得
    private val fileLauncherGetDirDisplayUri = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult() )
    { result -> // 結果を受け取るルーチン
        if (result.resultCode == RESULT_OK) {
            // succeeded.
            // get uri of directory
            val selectedDirUri = result.data?.data ?: throw  Exception()

            /*
                uri冒頭の"content://com.android.externalstorage.documents/tree"を削除し、
                エンコードされた /（スラッシュ）を戻す。
                ContentPreviderに合わせ、VolumeとRelativeの文字列を取得する。
                プライマリ・ストレージ文字列は"/external_primary/"に置き換える。
             */
            val selectedDirString = selectedDirUri.toString().
            replaceFirst("content://com.android.externalstorage.documents/tree", "")

            selectedVolumeDir = selectedDirString.
            substring(0, selectedDirString.indexOf("%3A")+3 )
            if (selectedVolumeDir.indexOf("/primary%3A") > -1) {
                // primary volume
                selectedVolumeDir = selectedVolumeDir.replaceFirst("/primary%3A", "external_primary")

            } else if (selectedVolumeDir.indexOf("%3A") > -1) {
                // SD card
                selectedVolumeDir = selectedVolumeDir.replaceFirst("/", "").replaceFirst("%3A", "")
            }

            selectedRelativeDir = selectedDirString.
            substring( selectedDirString.indexOf("%3A")).
            replace("%3A", "" )
            if (selectedRelativeDir.indexOf("%2F") > -1) {
                selectedRelativeDir = selectedRelativeDir.replace("%2F", "/")
            }
            // Relativeの末尾を"/"にしておく
            if (selectedRelativeDir.takeLast(1) != "/" ) {
                selectedRelativeDir = selectedRelativeDir + "/"
            }

            binding.textViewDir.text = getString(R.string.selected_path, selectedVolumeDir, selectedRelativeDir)

            sharedPref.edit().
                putString(getString(R.string.volume_dir_key), selectedVolumeDir).
                putString(getString(R.string.relative_dir_key), selectedRelativeDir).
                apply()
        } else {
            // failed
            binding.textViewDir.text = getString(R.string.no_dirs)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        appContext = applicationContext
        getFileReadPermission()
    }

    /*
     * パーミッションを取得するメソッド
     *   パーミッションが取得されれば
     *
     *   パーミッションが取得されなければ
     *
    */
    private fun getFileReadPermission() {
        if ( ContextCompat.checkSelfPermission(this, myPermission) ==
            PackageManager.PERMISSION_GRANTED) {
            // 既にパーミッションが取得されている時
            // パーミッション取得後にすべき処理に移る
            isCheckedNotAskAgain = false
            sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).apply()
            workWithPermission()
        } else {
            // パーミッション未取得
            if (isCheckedNotAskAgain) {
                // 過去に権限取得を拒否され、「今後表示しない」が選択されて再リクエストも拒否されている
                // ダイアログでユーザーに説明し、ユーザーが望めば設定画面を表示する
                val openConfigDialogFragment = OpenConfigDialogFragment()
                openConfigDialogFragment.show(supportFragmentManager, "simple")

                // ダイアログ後の処理
                if ( ContextCompat.checkSelfPermission(this, myPermission) ==
                    PackageManager.PERMISSION_GRANTED) {
                    // （設定画面で）パーミッションを取得している
                    isCheckedNotAskAgain = false
                    sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).
                        apply()
                    workWithPermission()
                } else {
                    // パーミッション未取得。
                    // shouldShowRequestPermissionRationale()に応じて
                    // isCheckedNotAskAgainを設定し保存する
                    if (shouldShowRequestPermissionRationale(myPermission)) {
                        // 「今後表示しない」は選択されていない
                        isCheckedNotAskAgain = false
                        sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).
                            apply()
                    } else {
                        // 「今後表示しない」が選択されている
                        isCheckedNotAskAgain = true
                        sharedPref.edit().putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain).
                            apply()
                    }
                    workWithoutPermission()
                }
            } else {
                // パーミッションを取得しておらず、
                // 過去に権限取得で再リクエストの拒否はしていない。
                permisLauncher.launch(myPermission)
            }
        }
    }

    // ファイル読み込みパーミッション取得後の処理
    private fun workWithPermission() {
        binding.textViewMediaPath.text = getString(R.string.havePermission)

        // get directory in external storage and display in screen.
        selectedVolumeDir = sharedPref.getString(getString(R.string.volume_dir_key), "") ?: throw Exception()
        selectedRelativeDir = sharedPref.getString(getString(R.string.relative_dir_key), "") ?: throw Exception()
        if (selectedVolumeDir.equals("") || selectedRelativeDir.equals("")) {
            // Open file-picker to get directory and display uri in screen.
            // You can get variables of selectedDirUri, selectedDirString.
            selectDirDisplayUri()
        } else {
            // SharedPreferrenceから取得したディレクトリ情報を利用する
            binding.textViewDir.text =
                getString(R.string.selected_path, selectedVolumeDir, selectedRelativeDir)
        }

        // ボタンがタップされたら、検索ディレクトリを取得し、スクリーンに表示する
        binding.buttonChangeDir.setOnClickListener {
            selectDirDisplayUri()
        }

        // select media type to be played from spinner in layout
        //     音声のみ、動画のみ、音声＋動画
        setAdapterSpinner()
        spinnerMediaType.onItemSelectedListener = CustomItemSelectedListener(this)

        // 再生
        binding.buttonPlay.setOnClickListener {
            binding.textViewMediaPath.text = ""
            binding.textViewFileSize.text = ""
            playMediaInDir(typeOfMedia)
        }

        // Replay
        binding.buttonReplay.setOnClickListener {
            val mediaIntent = Intent()
            mediaIntent.action = Intent.ACTION_VIEW
            // mediaType  0:none 1:image 2:audio 3:video 4:playlist 5:subtitle 6:document
            if (medType == 2) {
                mediaIntent.setDataAndType(Uri.parse(medPath), "audio/*")
            } else if (medType == 3) {
                mediaIntent.setDataAndType(Uri.parse(medPath), "video/*")
            } else {
                binding.textViewMediaPath.text = getString(R.string.illegal_typeMedia)
            }
            if (1 < medType && medType < 4) {
                startActivity(mediaIntent)
            }
        }
    }

    private fun setAdapterSpinner() {
        val spinnerAdapter = ArrayAdapter.createFromResource(
            this, R.array.spinner_items_media_type, android.R.layout.simple_spinner_item)
            // simple_spinner_item
            //     プラットフォームによってデフォルトで提供されているスピナーの外観レイアウト
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // simple_spinner_dropdown_item
            //     プラットフォームで定義された標準のドロップダウンレイアウト
        spinnerMediaType.setAdapter(spinnerAdapter)
    }

    private fun workWithoutPermission() {
        binding.textViewMediaPath.text = getString(R.string.noPermission)
    }

    private fun selectDirDisplayUri() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION or
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION or
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION or
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION )
        }
        fileLauncherGetDirDisplayUri.launch(intent)
    }

    /*
     * Play media.
     * Int typeMedia: Type of media to play
     */
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun playMediaInDir(typeMedia: MediaMyEnumType) {
        lateinit var selectionClause: String

        /*
         * get uri of audio media and video media in the directory
         */
        val contentResolver = this.contentResolver
        val proj = arrayOf(
//            MediaStore.MediaColumns._ID,
            MediaStore.Files.FileColumns.MEDIA_TYPE,
//            MediaStore.Files.FileColumns.DISPLAY_NAME,
//            MediaStore.Files.FileColumns.RELATIVE_PATH,
//            MediaStore.Files.FileColumns.VOLUME_NAME,
            MediaStore.Files.FileColumns.SIZE,
            "_data"
        )

        when(typeMedia) {
            MediaMyEnumType.soundVideo -> { // audio & video
                selectionClause = "(" +
                        MediaStore.Files.FileColumns.MEDIA_TYPE + " = " +
                        MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO +
                        " OR " +
                        MediaStore.Files.FileColumns.MEDIA_TYPE + " = " +
                        MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO +
                        ")" +
                        " AND " +
                        "(" +
                        MediaStore.Files.FileColumns.VOLUME_NAME + " LIKE ? " +
                        " AND " +
                        MediaStore.Files.FileColumns.RELATIVE_PATH + " LIKE ? " +
                        ")"
            }
            MediaMyEnumType.sound -> { // audio
                selectionClause = "(" +
                        MediaStore.Files.FileColumns.MEDIA_TYPE + " = " +
                        MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO +
                        ")" +
                        " AND " +
                        "(" +
                        MediaStore.Files.FileColumns.VOLUME_NAME + " LIKE ? " +
                        " AND " +
                        MediaStore.Files.FileColumns.RELATIVE_PATH + " LIKE ? " +
                        ")"
            }
            MediaMyEnumType.video -> { // video
                selectionClause = "(" +
                        MediaStore.Files.FileColumns.MEDIA_TYPE + " = " +
                        MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO +
                        ")" +
                        " AND " +
                        "(" +
                        MediaStore.Files.FileColumns.VOLUME_NAME + " LIKE ? " +
                        " AND " +
                        MediaStore.Files.FileColumns.RELATIVE_PATH + " LIKE ? " +
                        ")"
            }
            else -> {throw java.lang.Exception(getString(R.string.illegal_typeMedia))}
        }

        val selectionArgs = arrayOf(selectedVolumeDir, "$selectedRelativeDir%")

        val query = contentResolver.query(
            MediaStore.Files.getContentUri("external"),
            //MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL),
            proj,
            selectionClause,
            selectionArgs,
            null
        )
        val numCount = query?.count
        query?.use { cursor ->
            if (cursor.count < 1) {
                binding.textViewMediaPath.text = getString(R.string.not_find)
            } else {
                cursor.moveToPosition(getRandomNum(numCount!!))
                medType =
                    cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE))
                val medSize =
                    cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.SIZE))
                medPath = cursor.getString(cursor.getColumnIndexOrThrow("_data"))

                // play media
                val mediaIntent = Intent()
                mediaIntent.action = Intent.ACTION_VIEW
                // mediaType  0:none 1:image 2:audio 3:video 4:playlist 5:subtitle 6:document
                if (medType == 2) {
                    mediaIntent.setDataAndType(Uri.parse(medPath), "audio/*")
                } else if (medType == 3) {
                    mediaIntent.setDataAndType(Uri.parse(medPath), "video/*")
                } else {
                    binding.textViewMediaPath.text = getString(R.string.illegal_typeMedia)
                }
                if (1 < medType && medType < 4) {
                    binding.textViewMediaPath.text = medPath
                    binding.textViewFileSize.text =
                        getString(R.string.kb, (medSize / 1000L).toString())
                    startActivity(mediaIntent)

                    // save audio-file-path to storage
                    lateinit var fw: FileWriter
                    val savePathFile =
                        File(appContext.filesDir, getString(R.string.pathHistoryFile))
                    if (savePathFile.exists()) {
                        fw = FileWriter(savePathFile, true)
                    } else {
                        fw = FileWriter(savePathFile)
                    }
                    try {
                        fw.write("$medPath,")
                    } catch(e: IOException) {
                        binding.textViewMediaPath.text = e.toString()
                    } finally {
                        try {
                            fw.close()
                        } catch(e: IOException) {
                            binding.textViewMediaPath.text = e.toString()
                        }
                    }
                }
            }
        }
        query?.close()
    }

    // 0以上、maxNum未満の範囲でランダムな数を1つ返す
    private fun getRandomNum(maxNum: Int): Int {
        if (maxNum < 1) {
            throw Exception()
        }
        val random = Random()
        return random.nextInt(maxNum)
    }

    // option menu
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.option, menu)
        return true
    }

    //Option menuのitemがクリックされた時の動作
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.playHistory -> {
                val playedFilesDialog = ListPlayedFilesDialogFragment()
                playedFilesDialog.show(supportFragmentManager, "simple")
            }
        }
        return super.onOptionsItemSelected(item)
    }
}