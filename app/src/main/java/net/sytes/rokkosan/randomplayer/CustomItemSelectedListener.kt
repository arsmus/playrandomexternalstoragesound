package net.sytes.rokkosan.randomplayer

import android.view.View
import android.widget.AdapterView

class CustomItemSelectedListener(private val mainActivity: MainActivity) :
    AdapterView.OnItemSelectedListener {
    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        when (mainActivity.spinnerMediaType.selectedItemPosition) {
            0 -> { mainActivity.typeOfMedia = MainActivity.MediaMyEnumType.soundVideo }
            1 -> { mainActivity.typeOfMedia = MainActivity.MediaMyEnumType.sound }
            2 -> { mainActivity.typeOfMedia = MainActivity.MediaMyEnumType.video }
            else -> { mainActivity.binding.textViewMediaPath.text =
                mainActivity.getString(R.string.illegal_typeMedia) }
        }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
        mainActivity.binding.textViewMediaPath.text = mainActivity.getString(R.string.not_selected)
    }
}
