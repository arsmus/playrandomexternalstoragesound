package net.sytes.rokkosan.randomsmbsoundplay

// 2022 Feb. 13.
// 2022 Feb. 05.

// Ryuichi Hashimoto

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.app.AlertDialog
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import net.sytes.rokkosan.randomplayer.R
import java.io.*

class ListPlayedFilesDialogFragment: DialogFragment(){
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // get Dialog
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val myDialogView = inflater.inflate(R.layout.played_files_list, null)

        // get array of played files from saved file
        var playedFiles = ""
        lateinit var br: BufferedReader
        try {
            val savePathFile = File(this.context?.filesDir, getString(R.string.pathHistoryFile))
            // savePathFile has only one line.
            // the line is filePath data in which each datum is separated by ','.
            if (savePathFile.exists()) {
                br = BufferedReader(FileReader(savePathFile))
                try {
                    playedFiles = br.readLine()
                } catch(e: IOException) {
                    playedFiles = e.toString()
                } finally {
                    try {
                        br.close()
                    } catch(e: IOException) {
                        playedFiles = playedFiles + " " + e.toString()
                    }
                }
            }
        } catch (e: Exception){
            playedFiles = playedFiles + " " + e.toString()
        }
        if (playedFiles == "") {
            playedFiles = getString(R.string.noHistory)
        }
        playedFiles = playedFiles.removeSuffix(",")
        val playedFileList = playedFiles.split(",").asReversed()

        // システムに組み込まれた"android.R.layout.simple_list_item_1"を介して、
        // playedFileListをArrayAdapterに関連づける
        val myAdapter =
            ArrayAdapter(this.requireContext(), android.R.layout.simple_list_item_1, playedFileList)

        // ArrayAdapterをダイアログ上のレイアウト内のListViewにセットする
        val listViewFiles = myDialogView.findViewById<ListView>(R.id.listViewFiles)
        listViewFiles.adapter = myAdapter

        // display dialog
        builder.setView(myDialogView)
            .setTitle("Played Files")
            .setPositiveButton("Clear") { dialog, which ->
                lateinit var fos: FileOutputStream
                try {
                    val savePathFile =
                        File( context?.filesDir, getString(R.string.pathHistoryFile))
                    fos = FileOutputStream(savePathFile)
                    fos.write(','.code.toInt())
                } catch (e: Exception) {
                    Toast.makeText(context,e.toString(), Toast.LENGTH_LONG ).show()
                } finally {
                    fos.close()
                }
            }
            .setNeutralButton("OK") { _, _ ->
                // nothing is done
            }
        return builder.create()
    }
}